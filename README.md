# Templates

## Verscheidene Code snippets

- [Tabellen](tables.tex)
- [Koma Glossary](glossary.tex)


## Links

- [Paper Tips und Tricks](https://github.com/Wookai/paper-tips-and-tricks)
- [Betterbib](https://github.com/nschloe/betterbib)
- [Blacktex](https://github.com/nschloe/blacktex)
- [Don'ts](http://mirrors.ctan.org/info/l2tabu/english/l2tabuen.pdf)
- [matplotlib zu latex tikz](https://github.com/nschloe/tikzplotlib)

## Tables
![Better table formatting](tables.gif)

### CSV to tables

Look at https://tex.stackexchange.com/a/226629

## Footnotes

To use footnotes just do `\footnote{Text}`